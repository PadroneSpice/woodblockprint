#!/usr/bin/python

'''
Artist: Sean Castillo, a.k.a. HyDrone
Version 1.3 -> 7 March 2020
Date: 7 February 2016
Update: 13 February 2016
Description: This file takes user input and calls printWoodBlock.
'''

from printWoodBlock import *
from easterEggCheck import *
import time
import os

easterEgg = 0  # if 0, no easter egg is activated

inputCharA = input("Enter the first char: ")   # prompts user to enter a char for the water
inputCharB = input("Enter the second char: ")  # prompts user to enter another char for the water
inputCharC = input("Enter the triple char: ")  # prompts user to complete the trilogy of water
                  
if inputCharA == '':                   # if the user doesn't input anything,
    inputCharA = '~'                   # defaults to tilde (~)

if inputCharB == '':                   # if the user doesn't input anything,
    inputCharB = '`'                   # defaults to grave accent (`)

if inputCharC == '':                   # if the user doesn't input anything,
    inputCharC = 's'                   # he ain't ballin'.


# Easter Egg Inputs
easterEgg = easterEggCheck(inputCharA, inputCharB, inputCharC)

# Prints the woodblock print
printWoodBlock(inputCharA, inputCharB, inputCharC, easterEgg)
