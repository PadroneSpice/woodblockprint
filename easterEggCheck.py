#!/usr/bin/python

'''
Author: Sean Castillo
Version 1.3
Updated 7 March 2020
'''

def easterEggCheck(inputCharA, inputCharB, inputCharC):
    easterEgg = 0
    
    # Easter Egg Inputs
    if inputCharA.lower() == ("fiona apple"):
       easterEgg = 1
    if inputCharA.lower() == ("get shreked"):
       easterEgg = 2
    if inputCharA.lower() == ("5 dozen eggs"):
       easterEgg = 3
    if (inputCharA.lower() == ("monty") and
        inputCharB.lower() == ("oum") and
        inputCharC.lower() == ("dances")
       ):
       easterEgg = 4
    if (inputCharA.lower() == ("star") and
        inputChar.lower() == ("platinum") and
        inputCharC.lower() == ("says")
       ):
       easterEgg = 5
    if (inputCharA.lower() == ("ash") and
        inputCharB.lower() == ("vs") and
        inputCharC.lower() == ("evil dead")
       ):
       easterEgg = 6
    if (inputCharA.lower() == ("this party's") and
        inputCharB.lower() == ("gettin'") and
        inputCharC.lower() == ("crazy")
       ):
       easterEgg = 7
    if (inputCharA.lower() == ("i") and
        inputCharB.lower() == ("don't") and
        inputCharC.lower() == ("chuckle")
       ):
       easterEgg = 8
    if (inputCharA.lower() == ("morrigan") and
        inputCharB.lower() == ("jedah") and
        inputCharC.lower() == ("rikuo")
       ):
       easterEgg = 9

    return easterEgg

def getEasterEggText(easterEgg):
    easterEggText = ""

    # Easter Egg line prints
    # (Input: Line 1: 'fiona apple') (Lyrics to 'Criminal')
    secretLine1 = "What I need is a good defense, 'cause I'm feelin' like a criminal "
    secretLine1 += "And I need to be redeemed to the one I've sinned against because he's all I ever knew of love. "
    secretLine1 += "Let me know the way before there's Hell to pay Give me room to lay the law and let me go. "
    secretLine1 += "I've got to make a play to make my lover stay So what would an angel say The devil wants to know. " 

    # (Input: Line 1: 'get shreked') (Lyrics to 'All Star')
    secretLine2 = "SomeBODY once told me the world is gonna roll me. I ain't the sharpest tool in the sheeeeed. "
    secretLine2 += "She was looking kind of dumb with her finger and her thumb in the shape of an L on her foreheaad. "

    # (Input: Line 1: '5 dozen eggs') (Gaston gag)
    secretLine3 = "No one is an easter egg in an ASCII animation like Gaston! "

    # (Input: Line 1: 'monty' Line 2: 'oum' Line 3: 'dances') (Lyrics to 'Shine')
    secretLine4 = "Baby, it's time to make up your mind. I think that tonight is when our stars align. "
    secretLine4 += "Honey, it's time to leave the doubt behind. Take my hand 'cause you and I are gonna shine. "
 
    # (Input: Line 1: 'star' Line 2: 'platinum' Line 3: 'says') (Star Platinum ORA)
    secretLine5 = "ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA "

    # (Input: Line 1: 'ash' Line 2: 'vs' Line 3: 'evil dead') (Lyrics to 'Space Truckin')
    secretLine6 = "We had a lot of luck on Venus We've always had a ball on Mars Meeting all the groovy people "
    secretLine6 += "We've rocked the Milky Way so far We danced around Borealice We're space truckin' 'round the stars " 
    secretLine6 += "Come on! Come on! Come on! Let's go Space Truckin'! Come on! Come on! Come on! Space Truckin! "

    # (Input: Line 1: 'this party's' Line 2: 'gettin'' Line 3: "crazy") (Lyrics to 'Devils Never Cry')
    secretLine7 = "Steal a soul for a second chance But you will never become a man. My chosen torture makes me stronger "
    secretLine7 += "In a life that craves the hunger, A freedom and quest for life Until the end of the judgement night "
    secretLine7 += "Bless me with your gift of light, Righteous cause on judgement nights. Feel the sorrow that light has "
    secretLine7 += "swallowed, Feel the freedom like no tomorrow. Stepping forth, a cure for souls' demise. Reap the tears "
    secretLine7 += "of the victims' cries. Yearning more to hear the suffer of a demon as a put it under. Kill before a time "
    secretLine7 += "to kill them all Passed down the righteous law Serve a justice that dwells in me, Lifeless corpse as far "
    secretLine7 += "as the eye can see! "   

    # (Input: Line 1: 'i' Line 2: 'don't' Line 3: 'chuckle') (Lyrics to 'Unknown from M.E.')
    secretLine8 = "Here I come, rougher than the rest of them The best of them, tougher than leather "
    secretLine8 += "You can call me Knuckles Unlike Sonic I don't chuckle; I'd rather flex my muscles "
    secretLine8 += "My heart is nails; it ain't hard to tell I break 'em down whether they're solid or frail " 
    secretLine8 += "Unlike the rest I'm independent since my first breath First test, feel right, then the worst's left "

    # (Input: Line 1: 'morrigan' Line 2: 'jedah' Line 3: 'rikuo') (Lyrics to 'Trouble Man')
    secretLine9 = "Gonna be trouble It's gettin' out of hand Gonna be trouble But baby, I'm the man "
    secretLine9 += "I'm gonna save you I'll be your knight I'll be your saviour How 'bout tonight? YEAH! "
    secretLine9 += "Gonna be trouble Baby I'm the trouble man Want a fighter? Come on- But don't you understand? "
    secretLine9 += "I can't be double Baby, I'm the trouble man "

    if easterEgg == 1:
       easterEggText = secretLine1
    if easterEgg == 2:
       easterEggText = secretLine2
    if easterEgg == 3:
       easterEggText = secretLine3
    if easterEgg == 4:
       easterEggText = secretLine4
    if easterEgg == 5:
       easterEggText = secretLine5
    if easterEgg == 6:
       easterEggText = secretLine6
    if easterEgg == 7:
       easterEggText = secretLine7
    if easterEgg == 8:
       easterEggText = secretLine8
    if easterEgg == 9:
       easterEggText = secretLine9

    return easterEggText
