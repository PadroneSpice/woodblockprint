#!/usr/bin/python

'''
Artist: Sean Castillo, a.k.a. HyDrone
Version 1.3
Description: This file prints the Japanese woodblock-inspired
             ASCII art piece. The kanji says 'Gomi no Hi',
             which translates to 'Garbage Day'.
'''

import time
import os
from easterEggCheck import *

# maps the woodblock print piece
def printWoodBlock(inputCharA, inputCharB, inputCharC, easterEgg):
    line0 = list("                      _                                                                        ")
    line1 = list("                     / \_                                                                      ")
    line2 = list("                    /     \                                           ____                     ")
    line3 = list("          _       _/ ## ## #\ /#\                                 ____|##|_____                ")
    line4 = list("         / \     /  ## #### #\ ##\      /\                        ||_|____|__                  ")
    line5 = list("       /## # \ _|  ### #  # ##\####\   / #\                       |[_|____|__]|                ")
    line6 = list("    _ /#####/# /-#### ##### # ##\___\_/## #\   /\                 |[_|____|__]|                ")
    line7 = list("  / ######/####\#### ################/#### ##\/__\__/\            || [__  |, ]|                ")
    line8 = list(" |##############\##### ###############\#### ##\## ####\           ||_|,   [_|]|                ")
    line9 = list(" /############################________________________ ##/\ /-\   ||____||____                 ")
    line10 =list("#####/\%%%\%%%\%%%\,______###/|=====|==========|======|\###/###\  / ____||____                 ")
    line11 =list("####/%%\%%%\%%%\%%%\,---\-/|/=|_____|__________|______|=\ ######|                              ")
    line12 =list("###/\%%%\%%%\%%%\%%%\,---/*|=/|=====|==========|======|==\_______      ___                     ")
    line13 =list("##/%%\%%%\%%%\%%%\%%%\,-/|/|/ |/////|//////////|//////|///-------     (_) )                    ")
    line14 =list("#/%%%%\%%%\%%%\%%%\%%%\/*/ |  |~~``s|s``~~sssss|~~```~|~~`\______ __         __                ")
    line15 =list(" |*********************|/|/|>V|~~~``|ss```~~sss|~````~|~`~~sss\///||_________||                ")
    line16 =list(" |          ____       | / |,,>V~~~~^`sss```sss^~ss`~~^~sssssss|__||         ||                ")
    line17 =list(" |         I__|_I      |/|/|,,,>Vsssss~sss````sss``ssss~~ssssss```||_________||                ")
    line18 =list(" |      ___I__|_I      | / |,,>Vsss`~~ss~sssssssssss~~~~~~~sss````||#########||                ")
    line19 =list(" |     /\%%\%%\%\      |/|/,>,V~sss~~~ssss~~~ss~~ss~~~sss~~~s~~```||         ||                ")
    line20 =list(" |    /%%\%%\%%\%/     | /,>,Vss`~```~~~sssss~~~~ssss~~`````ss~~``||_________||                ") 
    line21 =list(" |___/%%%%\%%\%%/ |____|/,>,VVs`ss~~~ss`sssss~~ss`~ssss~~~`ssss~~`||#########||                ") 
    line22 =list("< ###|**********| |,,,,,,>,VVs``~ss~~~sssssssss~~~~``~~~`sssssss~~||         ||                ") 
    line23 =list("< ###|    __    | |,,,,,>,^V~s``ssss~~~sssssss~~`sss~~```^^^^^^ss~;;         ;;                ")
    line24 =list(",< ##|   |  |   | |,,,,>,,^Vsssssssss~~ssssss~````s~~s^^^,,,,,,^^^>                            ") 
    line25 =list("^,< #|___|__|___|/,,,,,>,^Vsss```~~sss~~~ss~~``~~~~ss<,,,,,,,,,,,,,>                           ")
    line26 =list("V^,<,,,,,,,,,,,,,,,,,,>,^Vss~~~```~~~```~~~``~~~ssss<,,,,,,,,,,,,,,,>                          ")
    line27 =list(" V^,<,,,,,,,,,,,,,,,>,^^Vs~~s`````~ss~~~`~~~~`sssss<,,,,,,,,,,,,,,,,,>                         ")
    line28 =list("  V^,<,,,,,,,,,,,,,,^^VVs~~sss~~``ssss`~~~~~~sssss<,,,,,,,,,,,,,,,,,,,,>                       ")
    line29 =list("   V^,^^^^^^^^^^>,^^^Vss~~~~ssssssssssss```ssssss<,,,,,,,,,,,,,,,,,,,,,,>                      ")

    # determines textlen and secretLineToUse
    secretLineToUse = ""
    secretLineToUse = getEasterEggText(easterEgg) 
    textlen = len(secretLineToUse)
      
    # combines the lines into a single list for printing
    listyList = [line0, line1, line2, line3, line4, line5, line6, line7, line8, line9,
                 line10, line11, line12, line13, line14, line15, line16, line17, line18,
                 line19, line20, line21, line22, line23, line24, line25, line26, line27, 
                 line28, line29]

    # sets the boolean statements
    regularPrint = True      # True that a regular print should be made
    easterEggPrint = False   # False that an easter egg print should be made
                             # It's designed this way to default to regular print.

    # applies condition if one of the easter eggs is activated
    if (easterEgg > 0):
           regularPrint = False     # deactivates regular print
           easterEggPrint = True    # activates easter egg print

    # infinite loop for printing a regular animated picture
    # cycles through 12 distinct frames, each exposed for 0.1337 seconds
    while regularPrint:
        # prints combo 1 (1, 2, 3) 
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '~':
                    item = inputCharA
                if item == '`':
                    item = inputCharB
                if item == 's':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 1.5 (' ', 2, 3)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '~':
                    item = ' '
                if item == '`':
                    item = inputCharB
                if item == 's':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)


        # prints combo 2 (2, 3, 1)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '`':
                    item = inputCharA
                if item == 's':
                    item = inputCharB
                if item == '~':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 2.8 (2, ' ', 1)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '`':
                    item = inputCharA
                if item == 's':
                    item = ' '
                if item == '~':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 3 (3, 1, 2)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == 's':
                    item = inputCharA
                if item == '~':
                    item = inputCharB
                if item == '`':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 3.6 (3, 1, ' ')
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == 's':
                    item = inputCharA
                if item == '~':
                    item = inputCharB
                if item == '`':
                    item = ' '
                txt += str(item)
            print(txt)
        time.sleep(0.1337)


        # prints combo 4 (2, 1, 3)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '`':
                    item = inputCharA
                if item == '~':
                    item = inputCharB
                if item == 's':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)
   
        # prints combo 4.13 (2, ' ', 1)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '`':
                    item = inputCharA
                if item == 's':
                    item = ' '
                if item == '~':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 5 (1, 3, 2)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '~':
                    item = inputCharA
                if item == 's':
                    item = inputCharB
                if item == '`':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 5.4 (' ', 3, 2)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == '~':
                    item = ' '
                if item == 's':
                    item = inputCharB
                if item == '`':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 6 (3, 2, 1)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == 's':
                    item = inputCharA
                if item == '`':
                    item = inputCharB
                if item == '~':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

        # prints combo 6 (3, ' ', 1)
        os.system('clear')
        for aList in listyList:
            txt = ''
            for item in aList:
                if item == 's':
                    item = inputCharA
                if item == '`':
                    item = ' '
                if item == '~':
                    item = inputCharC
                txt += str(item)
            print(txt)
        time.sleep(0.1337)

    # infinite loop for printing an easter egg print
    # still need to add movement, perhaps by increment loop
    while easterEggPrint:
       textCounter = 0
       textScroller = 0
       os.system('clear')
       for aList in listyList:
           txt = ''

           # experiment for scrolling mechanic; replaces char with blank space
           for item in aList:
               x = 0
               while x < textScroller:
                  if (item == '~' or item == '`' 
                      or item == 's'):  
                      item = ' '
           
           # replaces char with question mark
           for item in aList:
               if (item == '~' or item == '`' 
                   or item == 's'):  
                  item = '?'

               # replaces question mark with easterEgg char
               if item == '?':
                  item = secretLineToUse[textCounter]
                  textCounter += 1
               txt += str(item)
               if textCounter == textlen:
                  textCounter = 0     # resets textCounter
           # prints result text
           print(txt)
       time.sleep(0.1337)
       textScroller += 1
       if textScroller > 10:
          textScroller = 0
