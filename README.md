## Japanese Style Woodblock Print
by Sean Castillo a.k.a. HyDrone

Version 1.3

How to run:
    This program is meant to run in a Linux/Unix Terminal.
    It uses the clear() command, which is invalid in Windows command prompt.

    Type 'python3 main.py'

    (Make sure to enter the 3; 'python main.py' 
    does not work as this is designed for Python 3.x.)

How to use:

    Type a character and press 'Enter' upon every prompt.
    The characters entered determine the appearance of
    the animated ASCII art water.

Files required to run:

    main.py
    printWoodBlock.py
    easterEggCheck.py

1.3 Update 7 March 2020:

	-Edited the README to be a proper markdown file
    and specify that this is a Linux program.
    -Swapped out two of the easter eggs for better ones.

Last Updates 1.2 11 February 2016:

    -Moved easter egg checking functionality from main.py to
    the new file easterEggCheck.py
    -Streamlined Easter Egg algorithm so it no longer has to
    check which easter egg to print every single water character
    -Renamed directory to 'Woodblock'
    -Prototype for Easter Egg prints functional;
    doesn't have scrolling but at least prints
